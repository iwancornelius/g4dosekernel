#include "PhantomSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4Track.hh"
#include "G4String.hh"
#include "RunAction.hh"
#include "G4RunManager.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
PhantomSD::PhantomSD(G4String name):G4VSensitiveDetector(name)
{
    G4RunManager* theRunManager =  G4RunManager::GetRunManager();
    myRunAction = (RunAction*)theRunManager->GetUserRunAction();
    edep=0;
    hitPosition.set(0,0,0);
    isPrimaryDose = true; //we assume that it's primary dose to begin   
    isScattered = false;
    edepForThisEvent = 0;
    KE=0;
}

PhantomSD::~PhantomSD(){
}

void PhantomSD::Initialize(G4HCofThisEvent* HCE)
{
    isPrimaryDose = true;//assume primary dose at start of event
    isScattered = false;
    edepForThisEvent = 0;
    //need to initalise interaction origin at beginning of event
    myRunAction->SetInteractionOrigin(G4ThreeVector(0,0,0));
    
}

G4bool PhantomSD::ProcessHits(G4Step* theStep,G4TouchableHistory*)
{
    edep = theStep->GetTotalEnergyDeposit();
    G4Track* theTrack = theStep->GetTrack();
        
    //tracking cuts to prevent stuck particles with step limiter
    KE = theTrack->GetKineticEnergy();
    if((KE <= 100*eV)&&(KE > 1*eV)){
        //G4cout << "particle below E threshold in phantom, killing now" << G4endl;
        edep += KE;//add kinetic energy to energy deposition
        theTrack->SetTrackStatus(fStopAndKill);   
    }
    
    if(theTrack->GetTrackID() == 1){//primary photon, will be same even after first interaction
        if(theTrack->GetCurrentStepNumber()==1){
            theTrack->SetTrackStatus(fSuspend);
            //G4cout << "primary suspended " << G4endl;
            if(!isScattered){//first interaction in phantom,
                isScattered=true;
                //  G4cout << "is scattered " <<isScattered<< G4endl;
            }
        } else {
            isPrimaryDose = false;
            //G4cout << "primary off  " << G4endl;
        }
    }
    //
    
    if(edep>0) {
        G4ThreeVector prePt(theStep->GetPreStepPoint()->GetPosition());
        G4ThreeVector hitposition(theStep->GetPostStepPoint()->GetPosition());
        if(!myRunAction->GetUseInteractionForcing()){//means we are not forcing interactios, so need to reposition
            //G4cout << "phantom sd origin "<< myRunAction->GetInteractionOrigin() << G4endl;
            //G4cout << "hit position " << hitposition <<  " edep " << edep << G4endl;
            //if we have the first step in the photon trajectory, need to set to zero
            hitposition -= myRunAction->GetInteractionOrigin();
            if (theStep->GetTrack()->GetParentID() == 0) //means we have a primary particle
                if(theStep->GetTrack()->GetTrackID() == 1) //
                    if(1 == theStep->GetTrack()->GetCurrentStepNumber()){//means this is the first step(first
                        hitposition = G4ThreeVector(0,0,0);
                    }
            //G4cout << "  " << hitposition << G4endl;
        }
        myRunAction->TallyPhantomHit(hitposition, edep, isPrimaryDose);
    }
    
    
    //G4cout << "track number " <<  theTrack->GetTrackID() << " " << theTrack->GetCurrentStepNumber() << G4endl;
   
    
    return true;
}

void PhantomSD::EndOfEvent(G4HCofThisEvent*){
    
   // G4cout << "Energy deposition for this event " << edepForThisEvent << G4endl;
    
}
