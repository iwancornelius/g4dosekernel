#include "DoseScorer.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"

DoseScorer::DoseScorer(){
    G4NistManager* nist_manager = G4NistManager::Instance();
    G4Material* water = nist_manager->FindOrBuildMaterial("G4_WATER");
    waterDensity = water->GetDensity();
    
    doseDistribution = new G4bPIDTHitsMap<G4double>();
    
    calcDoseKernel = false;//by default we calculate the point spread function of photon energy. ie., fractoin of photon energy deposited at position x,y,z
    NumberOfHistories=1;    
    primaryPhotonEnergy=0;
    kernelFilename="";
    RunID="1";//
    primaryAppendix="";//string to add to filename if it's a primary dose contribution
    primaryEnergyDeposition=0;
}

DoseScorer::~DoseScorer(){
    if(doseDistribution) delete doseDistribution;
}


void DoseScorer::PrintToFile(){}    
void DoseScorer::TallyHit(const G4ThreeVector&, G4double ){}
