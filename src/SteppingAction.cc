#include "SteppingAction.hh"

#include "G4SteppingManager.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"


SteppingAction::SteppingAction()
{
    G4RunManager* theRunManager =  G4RunManager::GetRunManager();
    myRA = (RunAction*)theRunManager->GetUserRunAction();
}

SteppingAction::~SteppingAction()
{;}

void SteppingAction::UserSteppingAction(const G4Step* step){
    //
    if(!myRA->GetUseInteractionForcing()){//if we are not modelling forced interactions
        //G4cout << "parent id " << step->GetTrack()->GetParentID() << G4endl;
        //G4cout << "track id " << step->GetTrack()->GetTrackID() << G4endl;
        //G4cout << "step number " << step->GetTrack()->GetCurrentStepNumber() << G4endl;
        if (step->GetTrack()->GetParentID() == 0) //means we have a primary particle
            if(step->GetTrack()->GetTrackID() == 1) //
                if(1 == step->GetTrack()->GetCurrentStepNumber()){//means this is the first step(first interaction)
                    //G4cout << "SteppingAction interaction origin " << step->GetPostStepPoint()->GetPosition() << G4endl;
                    myRA->SetInteractionOrigin(step->GetPostStepPoint()->GetPosition());
                }
    }
}
