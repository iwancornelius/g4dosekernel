#include "SphericalDoseScorer.hh"
#include "cnpy.h"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh" 

SphericalDoseScorer::SphericalDoseScorer(G4String val ){
    
    G4cout << "- initialising spherical scorer -" << G4endl;
    
    NumberRVoxels = 600;
    ScorerSizeR = 600*mm;
    
    NumberThetaVoxels = 180;
    ScorerSizeTheta = pi;//180 degrees for conical segments
    
    Rmin = 0; Rmax = ScorerSizeR;
    ThetaMin = 0; ThetaMax = pi;
    
    scorerRresolution = ScorerSizeR/float(NumberRVoxels);
    scorerThetaresolution = ScorerSizeTheta/float(NumberThetaVoxels);
    
    primaryAppendix = val;
    
    calcDoseKernel=false;
    
    G4cout << " number voxels r theta " << NumberRVoxels <<  " " << NumberThetaVoxels << G4endl;
    G4cout << " size (mm) r : (rad) theta " << ScorerSizeR << " " << ScorerSizeTheta << G4endl;
    G4cout << " resolution mm/voxel r rad/voxel theta " << scorerRresolution << " " << scorerThetaresolution  << G4endl;
}

SphericalDoseScorer::~SphericalDoseScorer(){
}

void SphericalDoseScorer::TallyHit(const G4ThreeVector& pos, G4double Edep){
    
    G4double x=pos.x(); G4double y=pos.y();G4double z=pos.z();
    G4int voxelIndex = 0;
    G4double r = sqrt(pow(x,2)+pow(y,2)+pow(z,2));//calc distance from origin
    G4double theta = 0;
    if (r > 0) theta = acos(z/r);//calc theta
    //
    voxelIndex = CalcIndex(r,theta);//flag to ensure overloaded function is called
    if (voxelIndex >= 0) 
        doseDistribution->add(voxelIndex,Edep);//otherwise outside of boundaries
    /*else  {
        G4cout << "hit outside boundaries " << G4endl;
        G4cout << "x y z " << x << " "<< y << " "<<z << " "<<   G4endl;
        G4cout << "r " << r << G4endl;
        G4cout << "theta " << theta << G4endl;
    }*/
}

//calculate hisotgram bin index given coordinates
G4int SphericalDoseScorer::CalcIndex(G4double R, G4double Theta){
    if((R < Rmin)||(Theta < ThetaMin)||(R >= Rmax)||(Theta >= ThetaMax)){
        return -1;
    }
    G4int ir = floor((R - Rmin) / scorerRresolution);
    G4int itheta = floor((Theta - ThetaMin) / scorerThetaresolution);
    return CalcIndex(ir,itheta);
}

G4int SphericalDoseScorer::CalcIndex(G4int ir, G4int itheta){
    return (ir*NumberThetaVoxels+itheta);
}

void SphericalDoseScorer::PrintToFile(){
    
    G4String fileName = "results/rThetaDoseKernel_"+primaryAppendix+"__"+RunID+".npy";
    //
    G4int gridSize = NumberRVoxels*NumberThetaVoxels;
    double * dose = new double[gridSize];
    G4double radius=0;
    G4double theta=0;
    G4double quantityOfInterest=0;
    G4double voxelVolume=0;
    G4double integral = 0;
    G4double totalFraction = 0;
    G4double backScatterFraction = 0;
    //
    G4cout << "normalising to " <<G4double(NumberOfHistories) << " events " << G4endl;
    //
    for(int r = 0; r < NumberRVoxels; r++) {
        radius = scorerRresolution * (G4double(r) + 0.5);//middle of element
        for(int t = 0; t < NumberThetaVoxels; t++) {
            theta = scorerThetaresolution * (G4double(t) + 0.5);//middle of element
            G4int idx = CalcIndex(r,t);//use overloaded method
            G4double * doseValue =  (*doseDistribution)[idx];
            if(doseValue){//means there was an entry
                quantityOfInterest = (*doseValue)/ G4double(NumberOfHistories);//norm to number primaries, units MeV
                //G4cout << "dose entry " <<quantityOfInterest<<G4endl;
                 totalFraction += quantityOfInterest;//fractional energy
                //check if we are past 90 degrees 
                if (theta > pi/2.){
                    backScatterFraction += quantityOfInterest;
                }
                if(calcDoseKernel){//if we calculate dose per primary particle
                    voxelVolume = 2*pi*pow(radius,2.0)*sin(theta)*scorerThetaresolution*scorerRresolution;//mm3 
                    quantityOfInterest = quantityOfInterest / (voxelVolume*waterDensity);//converts Edep to Dose
                    dose[idx] =  quantityOfInterest/(joule/kg);//Converts dose to Gy
                } else {//otherwise we calculate fractional energy deposition per primary particle
                    quantityOfInterest = quantityOfInterest / primaryPhotonEnergy;//norm to primary energy, no units                
                    dose[idx] = quantityOfInterest;//
                }
            }
            else
                dose[idx] = 0;
        }
    }
    //
    G4cout << "Beam energy is: " << primaryPhotonEnergy / MeV << " MeV" << G4endl;
    G4cout << "Total energy deposition is: " << totalFraction    << G4endl;
    G4cout << "Total energy fraction is: " << totalFraction / primaryPhotonEnergy   << G4endl;
    G4cout << "Backscatter energy fracton is: " << backScatterFraction / primaryPhotonEnergy   << G4endl;
    G4cout << "Primary energy fracton is: " << primaryEnergyDeposition / NumberOfHistories / primaryPhotonEnergy   << G4endl;
    
    G4cout << "E: "<< primaryPhotonEnergy / MeV 
    << " EFracTot: "<< totalFraction / primaryPhotonEnergy 
    << " EFracBack: " << backScatterFraction / primaryPhotonEnergy 
    << " EFracPrim: " << primaryEnergyDeposition / NumberOfHistories / primaryPhotonEnergy 
    << G4endl;
    
//
	const unsigned int shape[] = {NumberRVoxels,NumberThetaVoxels};
	cnpy::npy_save(fileName,dose,shape,2,"w");
//
    delete [] dose;
}
