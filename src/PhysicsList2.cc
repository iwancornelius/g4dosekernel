// USER//
#include "PhysicsList2.hh"

// GEANT4 //
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmDNAPhysics.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmProcessOptions.hh"
#include "G4ParticleTypes.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4VEmProcess.hh"
#include "G4EmDNAPhysics.hh"
#include "G4StepLimiter.hh"
#include "G4SystemOfUnits.hh"
PhysicsList2::PhysicsList2()
{
    //RegisterPhysics(new G4EmDNAPhysics());
    RegisterPhysics(new G4EmStandardPhysics_option4());
    //RegisterPhysics(new G4EmLivermorePhysics());
    //RegisterPhysics(new G4EmPenelopePhysics());//best matching to EGS4 data with penelope models
    SetVerboseLevel(1);
    useInteractionForcing=false;
}

PhysicsList2::~PhysicsList2()
{;}

void PhysicsList2::ConstructParticle()
{
    G4VModularPhysicsList::ConstructParticle();
}

void PhysicsList2::ConstructProcess()
{
    G4VModularPhysicsList::ConstructProcess();
    //if we are using the run mode that implmenets forced interactions, do this
    //otherwise ignore and set cuts
    if(useInteractionForcing){
        //get physics manager for gamma
        //activate forced interactions for each gamma process, willwork regardless of what physics list is being used
        G4ProcessManager* pm = G4Gamma::GammaDefinition()->GetProcessManager();
        G4double numberGammaProcesses = pm->GetProcessListLength();//number processes for gammas
        for(unsigned int j=0;j<numberGammaProcesses;j++){
            G4ProcessVector* processVector = G4Gamma::Gamma()->GetProcessManager()->GetProcessList();
            G4VEmProcess* process = (G4VEmProcess*)processVector->operator [](j);//tricky, we get the process, G4VProcess, and typecast to derived class G4VEmProcess
            if(process->GetProcessType()==fElectromagnetic){//only if it's electro
                process->ActivateForcedInteraction( 1*nanometer,"forced");
            }
        }
    }
    
    G4EmProcessOptions em;
    em.SetApplyCuts(true); //characteristic x-ray emission 
    em.SetFluo(true);
    em.SetAuger(true);
    em.SetPIXE(true);
    em.SetVerbose(0);    
    
    //add step limiter for electrons
    G4StepLimiter* stepLimiter = new G4StepLimiter();
    G4ProcessManager* pm = G4Electron::ElectronDefinition()->GetProcessManager();
    pm->AddDiscreteProcess(stepLimiter);

}

void PhysicsList2::SetCuts()
{
    G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(250*eV, 1*GeV);//extent low energy limit to models
    SetCutValue(100 * micrometer, "gamma");
    SetCutValue(100 * micrometer, "e-");//approx 10 keV as per EdkNRC sim
    SetCutValue(100 * micrometer, "e+");
    if (verboseLevel>0) DumpCutValuesTable();
}

