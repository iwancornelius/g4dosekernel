//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//

#include "StackingAction.hh"

#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4HCofThisEvent.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"

StackingAction::StackingAction()
{ 
}

StackingAction::~StackingAction()
{}

G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track * aTrack)
{
  G4ClassificationOfNewTrack classification = fUrgent;
    
    if (aTrack->GetTrackStatus()==fSuspend){
        
        //G4cout << "Changing classification to waiting" << G4endl;
        classification = fWaiting;
        
    }
    
    /*
  switch(stage)
  {
  case 0: // Stage 0 : Primary muons only
    if(aTrack->GetParentID()==0)
    {
      G4ParticleDefinition * particleType = aTrack->GetDefinition();
      if((particleType==G4MuonPlus::MuonPlusDefinition())
       ||(particleType==G4MuonMinus::MuonMinusDefinition()))
      { classification = fUrgent; }
    }
    break;

  case 1: // Stage 1 : Charged primaries only
          //           Suspended tracks will be sent to the waiting stack
    if(aTrack->GetParentID()!=0) { break; }
    if(aTrack->GetTrackStatus()==fSuspend) { break; }
    if(aTrack->GetDefinition()->GetPDGCharge()==0.) { break; }
    classification = fUrgent;
    break;

  default: // Stage 2 : Accept all primaries
           //           Accept all secondaries in RoI
           //           Kill secondaries outside RoI
    if(aTrack->GetParentID()==0)
    { 
      classification = fUrgent;
      break;
    }
    if((angRoI<0.)||InsideRoI(aTrack,angRoI))
    { 
      classification = fUrgent;
      break;
    }
    classification = fKill;
  }
  */
  return classification;
}


void StackingAction::NewStage()
{
    
}
    
void StackingAction::PrepareNewEvent()
{
    
    
}


