#include "CartesianDoseScorer.hh"
#include "cnpy.h"

#include "G4SystemOfUnits.hh"

CartesianDoseScorer::CartesianDoseScorer(G4String val){
    
    G4cout << "- initialising cartesian scorer -" << G4endl;
    
    NumberXVoxels = 150; NumberYVoxels = 150; NumberZVoxels = 150;
    ScorerSizeX = 300 * mm; ScorerSizeY = 300 * mm; ScorerSizeZ = 300 * mm;
    primaryAppendix = val;
    Xmin = -ScorerSizeX/2; Xmax = ScorerSizeX/2;
    Ymin = -ScorerSizeY/2; Ymax = ScorerSizeY/2;
    Zmin = -ScorerSizeZ/2; Zmax = ScorerSizeZ/2;
    scorerXresolution = ScorerSizeX/float(NumberXVoxels);
    scorerYresolution = ScorerSizeY/float(NumberYVoxels);
    scorerZresolution = ScorerSizeZ/float(NumberZVoxels);
    G4cout << " number voxels x y z" << NumberXVoxels << " " << NumberYVoxels  << " " << NumberZVoxels << G4endl;
    G4cout << " size (mm) x y z" << ScorerSizeX << " " << ScorerSizeY  << " " << ScorerSizeZ << G4endl;
    G4cout << " resolution mm/voxel x y z" << scorerXresolution << " " << scorerYresolution  << " " << scorerZresolution << G4endl;
}

CartesianDoseScorer::~CartesianDoseScorer(){
    
}

void CartesianDoseScorer::TallyHit(const G4ThreeVector& pos, G4double Edep){
    G4double x=pos.x(); G4double y=pos.y();G4double z=pos.z();
    G4int voxelIndex = 0;
    voxelIndex = CalcIndex(x,y,z);
    if (voxelIndex >= 0) doseDistribution->add(voxelIndex,Edep); 
}

G4int CartesianDoseScorer::CalcIndex(G4double X, G4double Y, G4double Z){
    if((X < Xmin)||(Y < Ymin)||(Z < Zmin)||(X >= Xmax)||(Y >= Ymax)||(Z >= Zmax)){
        return -1;
    }
    G4int ix = floor((X - Xmin) / scorerXresolution); 
    G4int iy = floor((Y - Ymin) / scorerYresolution);
    G4int iz = floor((Z - Zmin) / scorerZresolution);
    return CalcIndex(ix,iy,iz); 
}

G4int CartesianDoseScorer::CalcIndex(G4int ix, G4int iy, G4int iz){
    return (ix*(NumberZVoxels*NumberYVoxels)+iy*NumberZVoxels+iz);
}

void CartesianDoseScorer::PrintToFile(){
    
    G4String fileName = "results/xyzDoseKernel_"+primaryAppendix+"__"+RunID+".npy";
    G4int gridSize = NumberXVoxels*NumberYVoxels*NumberZVoxels;
    double * dose = new double[gridSize];
    G4cout << "Normalising dose to: " << NumberOfHistories << " particles " << G4endl;
    G4double quantityOfInterest=0;
    G4double voxelVolume = scorerZresolution*scorerXresolution*scorerYresolution;
    G4double integral = 0;
    for(int x = 0; x < NumberXVoxels; x++) {
        for(int y = 0; y < NumberYVoxels; y++) {
            for(int z = 0; z < NumberZVoxels; z++) {
                G4int idx = CalcIndex(x,y,z); 
                G4double * doseValue =  (*doseDistribution)[idx];
                if(doseValue){ 
                    quantityOfInterest = (*doseValue)/NumberOfHistories; 
                    if(calcDoseKernel){ 
                        quantityOfInterest = quantityOfInterest/(voxelVolume*waterDensity); 
                        dose[idx] =  quantityOfInterest/(joule/kg); 
                    }else {
                        quantityOfInterest = quantityOfInterest / primaryPhotonEnergy; 
                        integral += quantityOfInterest;
                        dose[idx] = quantityOfInterest;                    
                    }
                } else
                    dose[idx] = 0;
            }
        }
    }
    G4cout << "Beam energy is " << primaryPhotonEnergy / MeV << " MeV" << G4endl;
    G4cout << "Integral of point spread function:  " << integral  << " A.U." << G4endl;

	const unsigned int shape[] = {NumberZVoxels,NumberYVoxels,NumberXVoxels};
	cnpy::npy_save(fileName,dose,shape,3,"w");


    delete [] dose;
}
