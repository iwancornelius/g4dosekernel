//
#include "DetectorConstruction.hh"
#include "PhantomSD.hh"

// 
#include "globals.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Region.hh"
#include "G4SDManager.hh"
#include "G4UserLimits.hh"
#include "G4SystemOfUnits.hh"
DetectorConstruction::DetectorConstruction()
{
    region=NULL;
    aPhantomSD=NULL; 
    stepLimit = NULL;
    useInteractionForcing=false;
}

DetectorConstruction::~DetectorConstruction()
{
    if(region) delete region;
    if(stepLimit) delete stepLimit;
}

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    G4NistManager* nist_manager = G4NistManager::Instance();
    G4Material* air = nist_manager->FindOrBuildMaterial("G4_AIR");
    G4Material* water = nist_manager->FindOrBuildMaterial("G4_WATER");
    world_solid = new G4Box("world_solid", 500*cm, 500*cm, 500*cm);
    world_logical = new G4LogicalVolume(world_solid, water, "world_logical", 0, 0, 0);
    world_physical = new G4PVPlacement(0, G4ThreeVector(), world_logical, "world_physical", 0, false, 0);
    
    //add STEP LIMITER to phantom volume, needed to ensure accuracy of scoring
    G4double maxStep = 0.1 * mm;
    stepLimit = new G4UserLimits();
    stepLimit->SetMaxAllowedStep(maxStep);        
    world_logical->SetUserLimits(stepLimit);
    
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    if(!aPhantomSD)  {
        aPhantomSD = new PhantomSD( "aPhantomSD" );
        SDman->AddNewDetector( aPhantomSD );
    }
    world_logical->SetSensitiveDetector( aPhantomSD );
    if(useInteractionForcing){
        G4double forcedVolX=2*nanometer;
        G4double forcedVolY=2*nanometer;
        G4double forcedVolZ=2*nanometer;
        G4Box * solidforcedVolZ = new G4Box("forcedVolZBox", forcedVolX/2., forcedVolY/2., forcedVolZ/2.);
        G4LogicalVolume* logicforcedVolZ = new G4LogicalVolume(solidforcedVolZ, water, "forcedVolZLog", 0, 0, 0);
        G4VPhysicalVolume* forcedVolZPhys = new G4PVPlacement(0,G4ThreeVector(),"forcedVolZPhys", logicforcedVolZ,  world_physical,0, false, 0);              // copy number
        region = new G4Region("forced");
        logicforcedVolZ->SetRegion(region);
        region->AddRootLogicalVolume(logicforcedVolZ);
        G4cout << "adding logicforcedVolZ to RegionOfInterest" << G4endl;
        logicforcedVolZ->SetSensitiveDetector( aPhantomSD );
    }
    return world_physical;
}

