#include "RunAction.hh"
#include "G4Run.hh"
#include "cnpy.h"
#include "RunActionMessenger.hh"
#include "PrimaryGeneratorAction.hh"

#include "CartesianDoseScorer.hh"
#include "SphericalDoseScorer.hh"
#include "DoseScorer.hh"

RunAction::RunAction(PrimaryGeneratorAction* pGA){
    useInteractionForcing=false;//by default, we do not consider forced interactions, instead we reposition hits
    myDoseScorer=NULL;
    SetScorer("cartesian");
    myRAM = new RunActionMessenger(this);
    interactionOrigin.set(0,0,0);//initliase interaction origin
    myPGA = pGA;//assign pointer to primary generator action class, to get energy for calculation of fractional energy impoarts
}

RunAction::~RunAction(){
}

void RunAction::SetScorer(G4String val) {
    
    if(myDoseScorer) delete myDoseScorer;
    
    if(val=="cartesian") {
        myDoseScorer = new CartesianDoseScorer("total");
    } else if(val=="spherical") {
        myDoseScorer = new SphericalDoseScorer("total"); 
    }
    else G4cout << "***UNKNOWN SCORER*** MUST BE: cartesian cylindrical or spherical" << G4endl;
    
}

void RunAction::BeginOfRunAction(const G4Run* aRun){
    G4int NumberOfHistories = aRun->GetNumberOfEventToBeProcessed();
    //
    myDoseScorer->SetNumberHistories(NumberOfHistories);
    myDoseScorer->ResetScorer();
    myDoseScorer->SetPrimaryPhotonEnergy(myPGA->GetPrimaryPhotonEnergy());
    //
    G4cout << "Processing " << NumberOfHistories << " events " << G4endl;
}

void RunAction::EndOfRunAction(const G4Run* aRun){
    myDoseScorer->PrintToFile(); 
    //myPrimaryDoseScorer->PrintToFile();
}

void RunAction::TallyPhantomHit(const G4ThreeVector& pos, G4double Edep, G4bool isPrimary){
    myDoseScorer->TallyHit(pos,Edep);//score total
    if(isPrimary) myDoseScorer->TallyPrimaryEdep(Edep);//if it's a 'primary' electron
}
