#include "RunActionMessenger.hh"
#include "RunAction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIdirectory.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithoutParameter.hh"

RunActionMessenger::RunActionMessenger(RunAction* RA){
    
    runAction = RA;
    
    Dir = new G4UIdirectory("/G4EnergyKernel/");
    Dir->SetGuidance("UI commands for the dose kernel calculator");
    
    
    runIDFilenameCmd = new G4UIcmdWithAString("/G4EnergyKernel/setRunID",this);
    runIDFilenameCmd->SetGuidance("set run identifier, will be used for file naming");
    runIDFilenameCmd->SetParameterName("name",false);
    runIDFilenameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
    setScorerCmd = new G4UIcmdWithAString("/G4EnergyKernel/setScorer",this);
    setScorerCmd->SetGuidance("set run identifier, will be used for file naming");
    setScorerCmd->SetParameterName("name",false);
    setScorerCmd->SetCandidates("cartesian spherical cylindrical");
    setScorerCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
    calcDoseKernelCmd = new G4UIcmdWithoutParameter("/G4EnergyKernel/calcDoseKernel",this);
    calcDoseKernelCmd->SetGuidance("calculate dose kernel, PSF calculated by default");
    calcDoseKernelCmd->AvailableForStates(G4State_Idle);
    
}

RunActionMessenger::~RunActionMessenger(){
    
    delete runIDFilenameCmd;
    delete setScorerCmd;
    delete calcDoseKernelCmd;
}

void RunActionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
    
    if (command == runIDFilenameCmd)
        runAction->SetRunID(newValue);
    else if (command == setScorerCmd)
        runAction->SetScorer(newValue);
    else if (command == calcDoseKernelCmd)
        runAction->CalculateDoseKernel();
    
    
}
