// USER //
#include "PrimaryGeneratorAction.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"



PrimaryGeneratorAction::PrimaryGeneratorAction()
{
  particle_gun = new G4ParticleGun();
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
    delete particle_gun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* event)
{
    particle_gun->GeneratePrimaryVertex(event);

}


G4double PrimaryGeneratorAction::GetPrimaryPhotonEnergy(){

    return particle_gun->GetParticleEnergy();

}

