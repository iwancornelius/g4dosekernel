#!/bin/bash
#an example of launching multiple simulations using a PBS queueing system

rm results/*
rm pbs/output/*

echo submitting job to PBS queue

for i in `seq 1 50`;
do
        qsub -o pbs/output -e pbs/output pbs/queueRun.pbs
done
