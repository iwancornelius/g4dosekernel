

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4VisExecutive.hh"
#include "RunAction.hh"
#include "SteppingAction.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList2.hh"
#include "PrimaryGeneratorAction.hh"
#include "StackingAction.hh"

int main(int argc,char** argv){

    G4RunManager* runManager = new G4RunManager();//class used for run control, a "run" generally considers a fixed geometry and finite number of primary particles

    G4bool useInteractionForcing = false;//by default, do not use interaction forcing. use offset method of Poole. 

    DetectorConstruction* geometry = new DetectorConstruction();//instantiating the class that defines geometry + materials
    geometry->SetUseInteractionForcing(useInteractionForcing);
    runManager->SetUserInitialization(geometry);//passes pointer to this class to the run manager class
    //
    PhysicsList2* physics = new PhysicsList2();//instantiating the class that defines particle types and processes
    physics->SetUseInteractionForcing(useInteractionForcing);
    runManager->SetUserInitialization(physics);
    //-------------------------------
    // UserAction classes, customize default behaviour of a Geant4 sim
    //-------------------------------
    PrimaryGeneratorAction* beam = new PrimaryGeneratorAction();//generation of primary particles. can be primary electrons or phase space photons
    runManager->SetUserAction(beam);//

    RunAction* runaction = new RunAction(beam);//define before physics list and detector construction as hard coding
    runaction->SetUseInteractionForcing(useInteractionForcing);
    runManager->SetUserAction(runaction);//mode of simulation (ie., forced interactions, or re-positioning

    SteppingAction* stepping_action = new SteppingAction();
    runManager->SetUserAction(stepping_action);
    
    StackingAction* stacking_action = new StackingAction;
    runManager->SetUserAction(stacking_action);


    runManager->Initialize();//calls all methods necessary to initialize event loop (geometry, physics tables, etc)
    G4UImanager * UI = G4UImanager::GetUIpointer(); //get pointer to user interface manager for macro commands
    G4VisManager* visManager = NULL;
    if (argc==1)   //no additional command-line arguments? ok then Define UI session for interactive mode
    {
        visManager = new G4VisExecutive;//ditto for visualisation manager
        visManager->Initialize();
        G4UIsession * session = 0;
        session = new G4UIterminal(new G4UItcsh);//simple UI session
        UI->ApplyCommand("/control/execute macros/vis.mac");//initiliase visualization based on macro file
        session->SessionStart();//start interactive session
        delete session;
    }
    else // there was a command line argument, which will contain a bunch of UI commands to execute... run in Byatch mode
    {
        G4String command = "/control/execute ";
        G4String fileName = argv[1];//the file
        UI->ApplyCommand(command+fileName);//derr
    }
    delete visManager;//clean up after yoself fool!
    delete runManager;//will automatically delete user classes as well
    return 0;
}
