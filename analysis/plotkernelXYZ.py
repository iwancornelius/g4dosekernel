import numpy
import matplotlib.pyplot as plt
from matplotlib import mpl
import os
import scipy 
import shlex

class plotkernelXYZ:
    def __init__(self):
        self.scorerSizeX=150.;self.scorerSizeY=150.;self.scorerSizeZ=150.
        self.scorerSizeR=600;#mm
        self.scorerSizeTheta=numpy.pi;#deg
        self.numberVoxelsX=0;self.numberVoxelsY=0;self.numberVoxelsZ=0
        self.numberVoxelsR=0;self.numberVoxelsTheta=0;
        self.totalNumberOfVoxels=0
        self.VoxelVolume=0
        self.DoseMatrix = []
        self.xzDosePlane=[]
        self.rzDosePlane=[]
        self.rthetaDosePlane=[]
        self.doseMax=0;self.doseMin=0
        self.Plot=1
        self.quantity='Dose'
        self.unit='Gy'
        #limits that will be used for trimming the dataset to match that of egs simulation
        #if these chagen prior to reading in data, then we will use the written value
        self.xmin=-1;self.xmax=-1;self.ymin=-1;self.ymax=-1;self.zmin=-1;self.zmax=-1;self.rmin=-1;self.rmax=-1; self.thetamin=-1;self.thetamax=-1
        self.xminInd=0;self.xmaxInd=0;self.yminInd=0;self.ymaxInd=0;self.zminInd=0;self.zmaxInd=0;self.rminInd=0;self.rmaxInd=0;self.thetaminInd=0;self.thetamaxInd=0
        #vectorize so that we may operate on arrays
        self.vecCoordToVoxel=numpy.vectorize(self.CoordToVoxel)
        self.vecVoxelToCoord=numpy.vectorize(self.VoxelToCoord)
        self.zDoseProfile=[];self.zValues=[];
        self.xDoseProfile=[];self.xValues=[];
        self.rDoseProfile=[];self.rValues=[];
        self.Xprofiles=[];self.Rprofiles=[];self.Zprofiles=[];self.ThetaProfiles=[]
        self.thetaValuesGrid=[];self.rValuesGrid=[];
        #
        self.cylindrical=False
        self.spherical=True
        self.cartesian=False
        self.saveFile=1
        self.profileThetaCoord=0
        self.profileRCoord=0
        self.Energies=[]
        self.mirrorRThetaPlane=0
        self.isEgs=0
        self.RelDiffPlot=0
        self.energy="1.25"
        self.dosekernel=0

    def setCoordSystem(self,system):
        self.cylindrical=False
        self.spherical=False
        self.cartesian=False
        if system == 'cartesian' : self.cartesian=True
        elif system == 'spherical' : self.spherical=True
        elif system == 'cylindrical' : self.cylindrical=True
        else:
            print ('invalid coord sysetm ',system)
            
    def convertToFloat32(self):
        self.DoseMatrix = numpy.float32(self.DoseMatrix)

    def calcAndPlotPolar(self, dirname):
        self.setCoordSystem('spherical')
        if self.isEg > 0 : loadEgsKernelFile(dirname+'/results/')
        else : self.loadG4KernelFiles(dirname+'/results/')
        self.setKernelLimits()
        self.calcRThetaPlane()
        self.calcRprofile()
        self.plotRprofiles()
        self.plotRThetaPlane()

    def plot(self):
        if self.Plot > 0: plt.show()

    def CoordToVoxel(self,coord, axis='r'):
        if axis == 'z':
            result=numpy.floor((coord + self.scorerSizeZ/2)*float(self.numberVoxelsZ)/self.scorerSizeZ)
        elif axis == 'x':
            result=numpy.floor((coord + self.scorerSizeX/2)*float(self.numberVoxelsX)/self.scorerSizeX)
        elif axis == 'y':
            result=numpy.floor((coord + self.scorerSizeY/2)*float(self.numberVoxelsY)/self.scorerSizeY)
        elif axis == 'r':
            result=numpy.floor((coord)*float(self.numberVoxelsR)/self.scorerSizeR)
        elif axis == 't':
            result=numpy.floor((coord)*float(self.numberVoxelsTheta)/self.scorerSizeTheta)
        else:
            print ('no axis ', axis)
        return int(result)

    def VoxelToCoord(self,voxel, axis='z'):
        if axis == 'z':
            result= -self.scorerSizeZ/2 + (float(voxel)+0.5)*self.scorerSizeZ/float(self.numberVoxelsZ)
        elif axis == 'x':
            result= -self.scorerSizeX/2 + (float(voxel)+0.5)*self.scorerSizeX/float(self.numberVoxelsX)
        elif axis == 'y':
            result= -self.scorerSizeY/2 + (float(voxel)+0.5)*self.scorerSizeY/float(self.numberVoxelsY)
        elif axis == 'r':
            result= (float(voxel)+0.5)*self.scorerSizeR/float(self.numberVoxelsR)
        elif axis == 't':
            result= (float(voxel)+0.5)*self.scorerSizeTheta/float(self.numberVoxelsTheta)
        else:
            print ('no axis ', axis)
        return result

    def loadG4KernelFiles(self, dirname, runID=''):#optional argument, if runID is specified, only opens filenames with that string in them
        print('loading G4 dose kernel from: ', dirname)
        allfiles=os.listdir(dirname)#get list of files in current directory
        # first check for existence of ExyzKernel.npy
        # this will be generated if resluts have already been collated
        self.saveFile=1
        # set default filenames
        if self.cartesian :
            self.KernelFilenameNpy='xyzDoseKernel__'+runID+'_MeV.npy'
            self.KernelFilenameCsv='ExyzKernel.csv'
            filekey = 'xyz'
        elif self.cylindrical :
            filekey = 'rz'
            self.KernelFilenameNpy='rzDoseKernel__'+runID+'.npy'
            self.KernelFilenameCsv='ErzKernel.csv'
        else:
            filekey = 'rTheta'
            self.KernelFilenameNpy='rThetaDoseKernel__'+runID+'.npy'
            self.KernelFilenameCsv='ErthetaKernel.csv'
        print(filekey, " ", self.KernelFilenameNpy, " " ,  self.KernelFilenameCsv)
        # now we see if we have already collated results from a previous run
        if self.KernelFilenameNpy in allfiles: #if the file exists
            self.DoseMatrix = numpy.load(dirname+self.KernelFilenameNpy) #read in the doseMatrix
            self.saveFile=0 # don't save the file, already been done
        else:#no pre-existing file, so we need to collate data
            numberofnpyfiles = 0#intialise file counter
            for item in allfiles:#for each file
                if 'npy' in item:#if we have an npy file
                    if filekey in item:#make sure it's the right type for this coord system
                        if runID in item:#make sure the run ID is in thestring also, if emptry, should be tru
                            if numberofnpyfiles == 0:#if ther first one, define the matrix
                                print("reading results from ", dirname+item)
                                self.DoseMatrix = numpy.load(dirname+item)
                            else:#otherwise accumulate to total
                                print("adding results from ", dirname+item)
                                tempDoseMatrix = numpy.load(dirname+item)
                                self.DoseMatrix += tempDoseMatrix # combine
                            numberofnpyfiles+=1#in either case, increment number of npy files
            print("Averaging over ", numberofnpyfiles)
            #now average over the number of files
            self.DoseMatrix /= float(numberofnpyfiles)
        #now calculate kernel limits
        self.setKernelLimits()
        self.integral = self.DoseMatrix.sum()
        print('total energy fraction is ', self.integral)
        #now we save the npy file so we don't have to read in all the kernel files next time
        if self.saveFile > 0 : self.saveResults(dirname)

    def GyToMeV(self,ValInGy):
        Density = 1e-3*1e6#kgm-3 for water
        Volume= self.xresolution*self.yresolution*self.zresolution*1e-9#volume in m3
        Mass = Density * Volume#in kg
        ValInGy *= Mass#convert to energy in joules
        MeVToJoules = 1e6*1.6e-19#1MeV = this many joules
        ValInGy /= MeVToJoules
        return ValInGy#now ValInMeV

    def setKernelLimits(self):
        # at this stage we have either read in a pre-collated edep matrix, or collated edep matrices from multiple files
        # calc scorer properties
        if self.cylindrical :
            self.numberVoxelsR=self.DoseMatrix.shape[0]
            self.numberVoxelsZ=self.DoseMatrix.shape[1]
            print ('numberVoxels ', self.numberVoxelsR,' ',self.numberVoxelsZ)
            if self.rmin < 0: self.rmin=0
            if self.rmax < 0: self.rmax=self.scorerSizeR/2
            if self.zmin < 0: self.zmin=-self.scorerSizeZ/2
            if self.zmax < 0: self.zmax=self.scorerSizeZ/2
            print("rmin ",self.rmin, " rmax ", self.rmax," zmin ",self.zmin, " zmax ", self.zmax)
            self.rminInd=self.CoordToVoxel(self.rmin,'r')
            self.rmaxInd=self.CoordToVoxel(self.rmax,'r')
            self.zminInd=self.CoordToVoxel(self.zmin,'z')
            self.zmaxInd=self.CoordToVoxel(self.zmax,'z')
            print( " rmin Ind",self.rminInd, " rmaxInd ", self.rmaxInd," zminInd ",self.zminInd, " zmaxInd ", self.zmaxInd)
        elif self.cartesian:
            #calculate the number of voxels in each dimension
            self.numberVoxelsX=self.DoseMatrix.shape[0]
            self.numberVoxelsY=self.DoseMatrix.shape[1]
            self.numberVoxelsZ=self.DoseMatrix.shape[2]
            print ('numberVoxels ', self.numberVoxelsX,self.numberVoxelsY,self.numberVoxelsZ)
            #calculate maximum edep
            #set default limits
            if self.xmin < 0: self.xmin=-self.scorerSizeX/2
            if self.xmax < 0: self.xmax=self.scorerSizeX/2
            if self.ymin < 0: self.ymin=-self.scorerSizeY/2
            if self.ymax < 0: self.ymax=self.scorerSizeY/2
            if self.zmin < 0: self.zmin=-self.scorerSizeZ/2
            if self.zmax < 0: self.zmax=self.scorerSizeZ/2
            print("xmin ",self.xmin, " xmax ", self.xmax, " ymin ",self.ymin, " ymax ", self.ymax," zmin ",self.zmin, " zmax ", self.zmax)
            self.xminInd=self.CoordToVoxel(self.xmin,'x')
            self.xmaxInd=self.CoordToVoxel(self.xmax,'x')
            self.yminInd=self.CoordToVoxel(self.ymin,'y')
            self.ymaxInd=self.CoordToVoxel(self.ymax,'y')
            self.zminInd=self.CoordToVoxel(self.zmin,'z')
            self.zmaxInd=self.CoordToVoxel(self.zmax,'z')
            print("xminInd ",self.xminInd, " xmaxInd ", self.xmaxInd, " ymin Ind",self.yminInd, " ymaxInd ", self.ymaxInd," zminInd ",self.zminInd, " zmaxInd ", self.zmaxInd)
            #calc resolution in mm
            self.xresolution = self.scorerSizeX / numpy.float(self.numberVoxelsX)
            self.yresolution = self.scorerSizeY / numpy.float(self.numberVoxelsY)
            self.zresolution = self.scorerSizeZ / numpy.float(self.numberVoxelsZ)
            self.totalNumberOfVoxels = self.numberVoxelsX*self.numberVoxelsY*self.numberVoxelsZ
            print("total number of voxels ", self.totalNumberOfVoxels)
            self.VoxelVolume = (self.xresolution*self.yresolution*self.zresolution)
            print("voxel volume ", self.VoxelVolume, " mm3")
            print("xresolution ",self.xresolution, " yresolution ", self.yresolution, " zresolution",self.zresolution)
        elif self.spherical:
            self.numberVoxelsR=self.DoseMatrix.shape[0]
            self.numberVoxelsTheta=self.DoseMatrix.shape[1]
            print ('numberVoxels ', self.numberVoxelsR,' ', self.numberVoxelsTheta)
            if self.rmin < 0: self.rmin=0
            if self.rmax < 0: self.rmax=self.scorerSizeR
            if self.thetamin < 0: self.thetamin=0
            if self.thetamax < 0: self.thetamax=self.scorerSizeTheta
            print("rmin ",self.rmin, " rmax ", self.rmax," thetamin ",self.thetamin, " zmax ", self.thetamax)
            self.rminInd=self.CoordToVoxel(self.rmin,'r')
            self.rmaxInd=self.CoordToVoxel(self.rmax,'r')
            self.thetaminInd=self.CoordToVoxel(self.thetamin,'t')
            self.thetamaxInd=self.CoordToVoxel(self.thetamax,'t')
            print( " rmin Ind ",self.rminInd, " rmaxInd ", self.rmaxInd," thetaminInd ",self.thetaminInd, " thetamaxInd ", self.thetamaxInd)
        #and calculate the dose extremes of the dataset
        self.doseMax=self.DoseMatrix.max()
        #get minimum non zero value of matrix
        self.doseMin=self.DoseMatrix[self.DoseMatrix > 0].min()
        print("Max dose ", self.doseMax," Min dose ", self.doseMin)


    def loadEgsKernelFile(self, dirname, runID):
        print('loading Egs dose kernel from: ', dirname)
        allfiles=os.listdir(dirname)#get list of files in current directory
        self.saveFile=1
        filekey = 'EgsrTheta'
        self.KernelFilenameNpy='EgsrThetaDoseKernel__'+runID+'.npy'
        print(filekey, " ", self.KernelFilenameNpy) 
        #now convert to format suitable for plotting
        self.numberVoxelsR = 600
        self.numberVoxelsTheta = 180
        if self.KernelFilenameNpy in allfiles: #if the file exists
            self.DoseMatrix = numpy.load(dirname+self.KernelFilenameNpy) #read in the doseMatrix
            self.saveFile=0 # don't save the file, already been done
        else:
            #introduce a test to see if .npy file exists... if so, read it in!
            f=open(dirname+runID+".egslst")
            data=f.readlines()
            #get index of the start of the data
            for i, item in enumerate(data):
                    if 'uncert' in item:
                            print item
                            break
            #get index of end of data
            for j, item in enumerate(data):
                    if 'END' in item:
                            print item
                            break
            #copy data to new array
            results=data[i+2:j-1]
            R=[0]
            Theta=[0]
            Dose=[0]
            for item in results:
                Theta.append(float(shlex.split(item)[0]))
                R.append(float(shlex.split(item)[1]))
                Dose.append(float(shlex.split(item)[2]))
            #we now have R, Theta, and Dose values. we need to generate x,y,z arrays for the dose values
            #get rid of leading zeros by slicing
            self.npR = numpy.array(R[1:])*10#convert to mm for dimensionsnpR - self.rRes / 2#place value at middle of bin
            self.npTheta = numpy.array(Theta[1:])#this is upper edge of theta bin
            self.npDose = numpy.array(Dose[1:])
            print(self.npDose.size)
            self.DoseMatrix = numpy.transpose(self.npDose.reshape(self.numberVoxelsTheta,self.numberVoxelsR))#reshape linear to matrix
        # may need to perform some transform on this IOT maintain orientation, row/col order as per G4 data
        if self.saveFile > 0 : self.saveResults(dirname)

    def calcXprofile(self,profileYcoord=0,sliceZcoord=0):
        sliceZind=self.CoordToVoxel(sliceZcoord, 'z')
        profileYind=self.CoordToVoxel(profileYcoord, 'y')
        self.Xprofiles.append(self.DoseMatrix[profileYind,:,sliceZind])
        xIndices=numpy.arange(0,self.numberVoxelsX)
        self.xValues=self.vecVoxelToCoord(xIndices, 'x')#can be overwritten no wukkas

    def plotXprofiles(self, imageFilename="xdosekernelprofiles.png"):
        fig=plt.figure()
        ax=fig.add_subplot("111", title="D(x)")
        for i,xprofile in enumerate(self.Xprofiles):
            ax.plot(self.xValues,xprofile, label="E="+self.Energies[i]+" MeV")#check here
        ax.set_ylim(ymin=0, ymax=self.doseMax)
        ax.set_xlabel('x, mm')
        ax.set_ylabel('Dose, Gy')
        if len(self.Xprofiles) >1 :plt.legend(loc=1)
        fig.savefig(imageFilename)

    def calcXZplane(self,sliceYcoord=0):
        sliceYind=self.CoordToVoxel(sliceYcoord, 'y')
        self.xzDosePlane=self.DoseMatrix[:,sliceYind,:]#DXZ
        #need to calculate newXgrid and newZgrid values
        resX=self.scorerSizeX/float(self.numberVoxelsX)
        resZ=self.scorerSizeZ/float(self.numberVoxelsZ)
        newX = numpy.arange(self.xmin,self.xmax,resX)
        newZ = numpy.arange(self.zmin,self.zmax,resZ)
        self.newZgrid, self.newXgrid = numpy.meshgrid(newZ,newX)

    def plotXZplane(self, imageFilename="xzdosekernelplane.png"):
        fig=plt.figure()
        ax=fig.add_subplot("111", aspect='equal')
        Norm=mpl.colors.LogNorm(vmin=self.doseMin,vmax=self.doseMax)
        cax=ax.pcolormesh(self.newXgrid, self.newZgrid, self.xzDosePlane, norm=Norm)
        ax.set_xlabel("x, mm")
        ax.set_ylabel("z, mm")
        cb=fig.colorbar(cax)
        cb.norm(self.doseMin,self.doseMax)
        cb.set_label("Dose, Gy")
        fig.savefig(imageFilename)
        
    def calcXYplane(self,sliceYcoord=0):
        sliceZind=self.CoordToVoxel(sliceYcoord, 'z')
        self.xyDosePlane=self.DoseMatrix[:,:,sliceZind]#DYX
        #need to calculate newXgrid and newZgrid values
        resX=self.scorerSizeX/float(self.numberVoxelsX)
        resY=self.scorerSizeY/float(self.numberVoxelsY)
        newX = numpy.arange(self.xmin,self.xmax,resX)
        newY = numpy.arange(self.ymin,self.ymax,resY)
        self.newYgrid, self.newXgrid = numpy.meshgrid(newY,newX)

    def plotXYplane(self, imageFilename="xydosekernelplane.png"):
        fig=plt.figure()
        ax=fig.add_subplot("111", title="D(x,y) E="+self.energy+" MeV",aspect='equal')
        Norm=mpl.colors.LogNorm(vmin=self.doseMin,vmax=self.doseMax)
        cax=ax.pcolormesh(self.newXgrid, self.newYgrid, self.xyDosePlane, norm=Norm)
        ax.set_xlabel("x, mm")
        ax.set_ylabel("y, mm")
        cb=fig.colorbar(cax)
        cb.norm(self.doseMin,self.doseMax)
        cb.set_label("Dose. Gy")
        fig.savefig(imageFilename)
        
    def calcZprofile(self,profileXcoord=0,profileYcoord=0):
        profileYind=self.CoordToVoxel(profileYcoord, 'y')
        profileXind=self.CoordToVoxel(profileXcoord, 'x')
        self.Zprofiles.append(self.DoseMatrix[profileXind,profileYind,:])
        zIndices=numpy.arange(0,self.numberVoxelsZ)
        self.zValues=self.vecVoxelToCoord(zIndices, 'z')

    def plotZprofiles(self):
        fig=plt.figure()
        ax=fig.add_subplot("111", title="D(z)")
        for i,zprofile in enumerate(self.Zprofiles):# will only occur for multiple energies
            ax.plot(self.zvalues,zprofile, label="E="+"%0.2f"%(selfEnergies[i])+" MeV")
        ax.set_ylim(ymin=0, ymax=self.doseMax)
        ax.set_xlabel('x, mm')
        ax.set_ylabel('Dose, Gy')
        if len(self.Xprofiles) >1 :plt.legend(loc=1)

    def calcRThetaPlane(self):#in this case we have read in a 2D dose matrix
        self.rThetaDosePlane=self.DoseMatrix#[self.rminInd:self.rmaxInd,self.thetaminInd:self.thetamaxInd]
        rIndices=numpy.arange(0,self.numberVoxelsR)
        self.rValues=self.vecVoxelToCoord(rIndices, 'r')
        thetaIndices=numpy.arange(0,self.numberVoxelsTheta)
        self.thetaValues=self.vecVoxelToCoord(thetaIndices, 't')
        self.thetaValuesGrid,self.rValuesGrid = numpy.meshgrid(self.thetaValues,self.rValues) #rectangular plot of polar data
        total = self.rThetaDosePlane.sum()
        self.backscatter = self.rThetaDosePlane[:,self.numberVoxelsTheta/2:self.numberVoxelsTheta].sum()
        self.backscatter /= total
        print ('total dose ', total)        
        print ('back scatter dose ', self.backscatter)
        #redefine backscatter as fraction of total dose in rearward direction
        #self.rThetaDosePlane[:,0:self.numberVoxelsTheta/2] = 0

    def cropRThetaPlane(self,Rmax):
        maxRindex = numpy.searchsorted(self.rValues,Rmax)
        self.scorerSizeR = Rmax
        croppedDosePlane = self.rThetaDosePlane[0:maxRindex,:]
        croppedRValues = self.rValuesGrid[0:maxRindex,:]
        croppedThetaValues = self.thetaValuesGrid[0:maxRindex,:]
        self.rValues = self.rValues[0:maxRindex]
        self.numberVoxelsR = maxRindex
        self.thetaValuesGrid=croppedThetaValues
        self.rValuesGrid=croppedRValues
        self.rThetaDosePlane=croppedDosePlane
        self.doseMax=self.DoseMatrix.max()
        self.doseMin=self.DoseMatrix[self.DoseMatrix > 0].min()
        print("Max dose ", self.doseMax," Min dose ", self.doseMin)

    def plotRThetaPlane(self,energyVal="", filename='rthetaplane.png'):
        labelStr=energyVal
        if energyVal != "": labelStr=" E= "+energyVal+ " MeV"
        fig=plt.figure()
        ax=fig.add_subplot("111", title="D(r,t)"+labelStr, polar='True')
        #ax.set_theta_zero_location('N')
        raxisvals = numpy.arange(self.scorerSizeR/5.0,self.scorerSizeR+self.scorerSizeR/5.0,self.scorerSizeR/5.0)
        print ('raxi ', raxisvals)
        plt.rgrids(raxisvals,angle=90)
        Norm=mpl.colors.LogNorm(vmin=self.doseMin,vmax=self.doseMax)
        cax=ax.pcolormesh(self.thetaValuesGrid,self.rValuesGrid , self.rThetaDosePlane, norm=Norm, label=labelStr) #X,Y & data2D must all be same dimensions
        cb=fig.colorbar(cax)
        cb.norm(self.doseMin,self.doseMax)
        if self.RelDiffPlot < 1 : cb.set_label("Dose. Gy")
        else : cb.set_label('$\Delta D / D_{EGS}$, % ')
        plt.savefig(filename)

    def calcRprofile(self):
        print("self.profileThetaCoord ",self.profileThetaCoord)
        profileThetaind=self.CoordToVoxel(self.profileThetaCoord, 't')
        if profileThetaind < 0 :
            print ('theta ind neg ')
            profileThetaind = 0
        print('theta ind ', profileThetaind)
        self.Rprofiles.append(self.rThetaDosePlane[:,profileThetaind])
        
    def calcThetaProfile(self):
        print("self.profileRCoord ",self.profileRCoord)
        profileRind=self.CoordToVoxel(self.profileRCoord, 'r')
        if profileRind < 0 :
            print ('r ind neg ')
            profileRind = 0
        print('r ind ', profileRind)
        self.ThetaProfiles.append(self.rThetaDosePlane[profileRind,:])

    def plotRprofiles(self, filename='rprofiles.png'):
        fig=plt.figure()
        ax=fig.add_subplot("111", title="D(r)")
        for i,rprofile in enumerate(self.Rprofiles):
            if self.Energies: labelStr=self.Energies[i]+" MeV"
            else : labelStr = ""
            print(labelStr)
            ax.semilogy(self.rValues,rprofile, label=labelStr)#check here
        ax.set_xlabel('r, mm')
        ax.set_ylabel('Dose, Gy')
        ax.set_ylim(ymin=self.doseMin, ymax=self.doseMax)
        if len(self.Rprofiles) >1 :plt.legend(loc=1)
        plt.savefig(filename)

    def saveResults(self, dirname):
        # save results in npy format (to avoid re-reading in all files again)
        # also save in csv file for reading in to G4bPID software
        numpy.save(dirname+self.KernelFilenameNpy, self.DoseMatrix)

    def parseMacroFile(self,macroFilename, commandOI="/control/foreach macros/runEnergy.mac Energy"):#command of interest
        # code to search for energies in macro file
        print('reading macro file from: ', macroFilename)
        f = open (macroFilename)
        commands = f.read()
        lines = commands.splitlines()
        x = [f.find(commandOI) for f in lines]
        for i,item in enumerate(x):
            if item >= 0:
                #i now stores index of command
                command = lines[i]
                energy = command.split(" ")[3:-1]
                break#there can be only one
        self.Energies = energy#keep as string, as we use it to search filenames
        print("run IDs", self.Energies)













