import sys
import matplotlib.pyplot as plt
from matplotlib import mpl
import numpy
import os
currentDir=os.getcwd()

sys.path.append(currentDir+"/../g4dosekernel/analysis/")#need path to plotkernelXYZ.py

#
# firstly process the geant4 results
#
from plotkernelXYZ import plotkernelXYZ

for Energy in ['0.1','1.0','10.0']:

    rmax = 100

    #get total energy kernel
    g4=plotkernelXYZ()
    g4.setCoordSystem('spherical')
    g4.Plot=0#don't plot results, just calculate
    g4.loadG4KernelFiles('./results/','_total__'+Energy+'_MeV')#pass energy string as runID
    g4.calcRThetaPlane()   
    g4.cropRThetaPlane(rmax) 
    g4.doseMax=1e-9
    g4.doseMin=1e-15
    g4.plotRThetaPlane(filename='rthetaplaneG4_'+Energy+'_MeV.png')
    #
    # now process the egs results
    #
    egs=plotkernelXYZ()
    egs.isEgs=1
    egs.Plot=0#don't plot results, just calculate
    egs.setCoordSystem('spherical')
    egs.loadEgsKernelFile("../../edknrc_dosekernel_60cm/","edknrc_"+Energy+"_60cm")
    egs.setKernelLimits()
    egs.calcRThetaPlane()
    egs.cropRThetaPlane(rmax)
    egs.doseMax=g4.doseMax#override maxima
    egs.doseMin=g4.doseMin
    egs.plotRThetaPlane(filename='rthetaplaneEGS_'+Energy+'_MeV.png')
    
      
    #
    # plotting D(r) profiles for various theta
    #
    fig=plt.figure(figsize=(21, 6))
    for i,profileThetaCoord in enumerate([45, 90, 135]):
        #calculate the radial profile
        egs.profileThetaCoord=numpy.deg2rad(profileThetaCoord)
        egs.calcRprofile()
        g4.profileThetaCoord=numpy.deg2rad(profileThetaCoord)
        g4.calcRprofile()
        ax=fig.add_subplot("13"+str(i+1))
        plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=None)
        ax.set_ylim(ymin=1e-16, ymax=egs.Rprofiles[0].max()*1.3)
        p1, = plt.semilogy(egs.rValues,egs.Rprofiles[len(egs.Rprofiles)-1], linestyle='--', color='r')
        p2, = plt.semilogy(g4.rValues,g4.Rprofiles[len(g4.Rprofiles)-1], linestyle='-', color='b')#), label='g4')#nb rprofiles is list of profiles, need last one to be added
        plt.title(r"%d"%(profileThetaCoord)+"$^o$")
        plt.ylabel("Dose, Gy")
        plt.xlabel("r, mm")
        #CALCULATE RELATIVE DIFFERENCE
        relDiffProf = numpy.abs((egs.Rprofiles[len(egs.Rprofiles)-1] - g4.Rprofiles[len(g4.Rprofiles)-1])/egs.Rprofiles[len(egs.Rprofiles)-1]*100)
        ax2 = plt.twinx()
        p3, = plt.plot(egs.rValues,relDiffProf,'k:')
        ax2.set_ylabel('$\Delta D/D_{EGS}$, %')
        plt.ylim(ymin=0,ymax=100)
        plt.legend([p1,p2,p3], ["egs", "g4", "diff"], loc=1)
    #fig.tight_layout(pad=5.0,w_pad=7.0)
    plt.savefig('profilesEGSG4_'+Energy+'_.png')
    
    #
    # plotting D(thetat) for various r
    #
    
    fig=plt.figure(figsize=(21, 6))
    for i,profileRCoord in enumerate([rmax/10,rmax/2]):
        #calculate the radial profile
        egs.profileRCoord=profileRCoord
        egs.calcThetaProfile()
        g4.profileRCoord=profileRCoord
        g4.calcThetaProfile()
        ax=fig.add_subplot("13"+str(i+1))
        plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=None)
        ax.set_ylim(ymin=1e-16, ymax=egs.ThetaProfiles[0].max()*1.3)
        p1, = plt.semilogy(egs.thetaValues,egs.ThetaProfiles[len(egs.ThetaProfiles)-1], linestyle='--', color='r')
        p2, = plt.semilogy(g4.thetaValues,g4.ThetaProfiles[len(g4.ThetaProfiles)-1], linestyle='-', color='b')#), label='g4')#nb rprofiles is list of profiles, need last one to be added
        plt.title(r"%d"%(profileRCoord)+", mm")
        plt.ylabel("Dose, Gy")
        plt.xlabel(r"$\theta$, rad")
        #CALCULATE RELATIVE DIFFERENCE
        relDiffProf = numpy.abs((egs.ThetaProfiles[len(egs.ThetaProfiles)-1] - g4.ThetaProfiles[len(g4.ThetaProfiles)-1])/egs.ThetaProfiles[len(egs.ThetaProfiles)-1]*100)
        ax2 = plt.twinx()
        p3, = plt.plot(egs.thetaValues,relDiffProf,'k:')
        ax2.set_ylabel('$\Delta D/D_{EGS}$, %')
        plt.ylim(ymin=0,ymax=100)
        plt.legend([p1,p2,p3], ["egs", "g4", "diff"], loc=1)
    #fig.tight_layout(pad=5.0,w_pad=7.0)
    plt.savefig('profilesThetaEGSG4_'+Energy+'_.png')
    
    
    #
    # create percentage relative difference rtheta image for the dataset
    #    
    
    relativeDifferenceMap = numpy.abs((g4.rThetaDosePlane - egs.rThetaDosePlane))/egs.rThetaDosePlane.copy()*100
    
    egs.rThetaDosePlane = relativeDifferenceMap #overwrite matrix in egs object,
    egs.doseMin = 1
    egs.doseMax = 1e2

    egs.RelDiffPlot=1
    egs.plotRThetaPlane(filename='relDifference'+Energy+'.png')#plot, should come up on new figure
    
    print('Median relative difference ', numpy.median(egs.rThetaDosePlane))
    print('Max relative difference ', numpy.max(egs.rThetaDosePlane))
    print('Min relative difference ', numpy.min(egs.rThetaDosePlane))
    print('Mean relative difference ', numpy.mean(egs.rThetaDosePlane))
    
