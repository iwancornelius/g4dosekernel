import sys
sys.path.append("../g4dosekernel/analysis/")
from plotkernelXYZ import plotkernelXYZ
p=plotkernelXYZ()
dirname="./"
p.setCoordSystem('cartesian')#only available with cartesian coord system
#re-implement this once UI commands are functioning
p.parseMacroFile('../g4dosekernel/macros/runCartesian.mac',"/control/foreach macros/runEnergy.mac Energy")#search for energies
#for each of the kernel energies
for energy in p.Energies:
    if energy is not '':
        print('combining .npy files with ID ', energy)
        p.loadG4KernelFiles('./results/', energy)#pass energy string as runID
        p.calcXZplane()
        p.plotXZplane("DoseXZ_"+energy+"_MeV_.png")
