import sys
import matplotlib.pyplot as plt
from matplotlib import mpl
import numpy
import os
currentDir=os.getcwd()

sys.path.append(currentDir+"/../G4EnergyKernel/analysis/")#need path to plotkernelXYZ.py

#
# firstly process the geant4 results
#
from plotkernelXYZ import plotkernelXYZ

Energy = '0.1'

#
g4Std=plotkernelXYZ()
g4Std.setCoordSystem('spherical')
g4Std.Plot=0#don't plot results, just calculate
g4Std.loadG4KernelFiles('../G4EnergyKernel-polar-std/results/',Energy+'_MeV')#pass energy string as runID
g4Std.calcRThetaPlane()

g4Penelope=plotkernelXYZ()
g4Penelope.setCoordSystem('spherical')
g4Penelope.Plot=0#don't plot results, just calculate
g4Penelope.loadG4KernelFiles('../G4EnergyKernel-polar-penelope/results/',Energy+'_MeV')#pass energy string as runID
g4Penelope.calcRThetaPlane()


g4Lawrence=plotkernelXYZ()
g4Lawrence.setCoordSystem('spherical')
g4Lawrence.Plot=0#don't plot results, just calculate
g4Lawrence.loadG4KernelFiles('../G4EnergyKernel-polar-ll/results/',Energy+'_MeV')#pass energy string as runID
g4Lawrence.calcRThetaPlane()

totalDoseGeant4Std=g4Std.rThetaDosePlane.sum()
print('Total dose: Standard ', totalDoseGeant4Std)

totalDoseGeant4Penelope=g4Penelope.rThetaDosePlane.sum()
print('Total dose: Penelope ', totalDoseGeant4Penelope)

totalDoseGeant4Lawrence=g4Lawrence.rThetaDosePlane.sum()
print('Total dose: Lawrence ', totalDoseGeant4Lawrence)

print ('Relative difference (%): ', numpy.abs(totalDoseGeant4Std-totalDoseGeant4Penelope)/totalDoseGeant4Std*100)

print ('Relative difference (%): ', numpy.abs(totalDoseGeant4Std-totalDoseGeant4Lawrence)/totalDoseGeant4Std*100)

#
# plotting D(r) profiles for various theta
#
fig=plt.figure(figsize=(21, 6))
for i,profileThetaCoord in enumerate([10,90,170]):
    g4Std.profileThetaCoord=numpy.deg2rad(profileThetaCoord)#calculate the radial profile
    g4Std.calcRprofile()
    g4Penelope.profileThetaCoord=numpy.deg2rad(profileThetaCoord)
    g4Penelope.calcRprofile()
    g4Lawrence.profileThetaCoord=numpy.deg2rad(profileThetaCoord)
    g4Lawrence.calcRprofile()
    ax=fig.add_subplot("13"+str(i+1))
    ax.set_ylim(ymin=g4Std.doseMin, ymax=g4Std.doseMax)
    p1, = plt.semilogy(g4Std.rValues,g4Std.Rprofiles[len(g4Std.Rprofiles)-1], linestyle='--', color='r')
    p2, = plt.semilogy(g4Penelope.rValues,g4Penelope.Rprofiles[len(g4Penelope.Rprofiles)-1], linestyle='-', color='b')#), label='g4')#nb rprofiles is list of profiles, need last one to be added
    p3, = plt.semilogy(g4Lawrence.rValues,g4Lawrence.Rprofiles[len(g4Lawrence.Rprofiles)-1], linestyle='-.', color='g')#), label='g4')#nb rprofiles is list of profiles, need last one to be added
    plt.title(r"%d"%(profileThetaCoord)+"$^o$")
    plt.ylabel("Dose, Gy")
    plt.xlabel("r, mm")
    #CALCULATE RELATIVE DIFFERENCE    
    ax2 = plt.twinx()
    relDiffProf =(g4Penelope.Rprofiles[len(g4Penelope.Rprofiles)-1] - g4Std.Rprofiles[len(g4Std.Rprofiles)-1])/g4Std.Rprofiles[len(g4Std.Rprofiles)-1]*100
    p4, = plt.plot(g4Std.rValues,relDiffProf,'k:')
    relDiffProf2 =(g4Lawrence.Rprofiles[len(g4Lawrence.Rprofiles)-1] - g4Std.Rprofiles[len(g4Std.Rprofiles)-1])/g4Std.Rprofiles[len(g4Std.Rprofiles)-1]*100
    p5, = plt.plot(g4Std.rValues,relDiffProf2,'k-.')
    #
    ax2.set_ylabel('$\Delta D/D_{G4STD}$, %')
    plt.ylim(ymin=-10,ymax=10)
    plt.legend([p1,p2,p3,p4,p5], ["g4 std", "g4 pen", "g4 liver", "diff pen", "diff livermore"], loc=1)
#fig.tight_layout(pad=5.0,w_pad=7.0)
plt.savefig('profilesG4StdVsPenVsLivermore.png')


