#!/bin/sh

# EnergyFractionsVsEnergy.py
# g4dosekernel
#
# Created by Iwan Cornelius on 15/11/12.
# Copyright 2012 iwan cornelius. All rights reserved.
# analyses all text files that have captured stdout of the simulation
# searches for energy, and total, primary, and backscatter energy fractions
# stores in list
# processes list to calculate average and std err of values for each energy
# plots against EdkNRC values
import os
import numpy
import matplotlib.pyplot as plt
from matplotlib import mpl
import sys
currentDir=os.getcwd()
sys.path.append(currentDir+"/../g4dosekernel/analysis/")#need path to plotkernelXYZ.py
from plotkernelXYZ import plotkernelXYZ

if __name__ == '__main__':

    ResDir = "./pbs/output/"
    
    #for each ".stdout" file in the directory
    allfiles=os.listdir(ResDir)#get list of files in current directory
    #lists to store energy, 
    energyList = []
    totalEnergyFractionList = []
    backScatterEnergyFractionList = []
    primaryEnergyFractionList = []
    #
    for item in allfiles:#for each file
        if 'OU' in item:#if we have a std out file
            f = open (ResDir+item)
            output = f.read()
            lines = output.splitlines()
            x = [f.find('E:') for f in lines]
            for i,index in enumerate(x):
                if index >= 0:
                    #i now stores index of command
                    command = lines[i]
                    energy = numpy.float(command.split(" ")[1:2][0]) 
                    energyList.append(energy)
                    totalEnergyFraction = numpy.float(command.split(" ")[3:4][0])
                    totalEnergyFractionList.append(totalEnergyFraction)
                    backScatterEnergyFraction = numpy.float(command.split(" ")[5:6][0])
                    backScatterEnergyFractionList.append(backScatterEnergyFraction)
                    primaryEnergyFraction = numpy.float(command.split(" ")[7:8][0])
                    primaryEnergyFractionList.append(primaryEnergyFraction)
    #we have lists of energies collected from all .stdout files here
    #create tuples for sorting                
    tuples=numpy.core.records.fromrecords(numpy.vstack((energyList,totalEnergyFractionList,backScatterEnergyFractionList,primaryEnergyFractionList)).transpose(), names='E,tEdep,bkEdep,prEdep')
    uniqueEnergies = numpy.sort(list(set(energyList)))#neat way to obtain unique values for energies
    #
    averageTotalEnergyFractions = []
    averageBackScatterEnergyFractions = []
    averagePrimaryEnergyFractions = []
    #
    stdErrTotalEnergyFractions = []
    stdErrBackScatterEnergyFractions = []
    stdErrPrimaryEnergyFractions = []
    #search list for each energy
    for energy in uniqueEnergies:
        thisEnergyTuples=tuples[tuples['E']==energy]#return all entries for this energy
        rootN = numpy.sqrt(thisEnergyTuples.size)
        averageTotalEnergyFractions.append(numpy.mean(thisEnergyTuples['tEdep']))
        stdErrTotalEnergyFractions.append(numpy.std(thisEnergyTuples['tEdep'])/rootN)
        averageBackScatterEnergyFractions.append(numpy.mean(thisEnergyTuples['bkEdep']))
        stdErrBackScatterEnergyFractions.append(numpy.std(thisEnergyTuples['bkEdep'])/rootN)
        averagePrimaryEnergyFractions.append(numpy.mean(thisEnergyTuples['prEdep']))
        stdErrPrimaryEnergyFractions.append(numpy.std(thisEnergyTuples['prEdep'])/rootN)
        
    #obtain total energy fraction from egslst files
    EgsDir = "../../edknrc_dosekernel_60cm/"
    allEgsFiles=os.listdir(EgsDir)
    egsEnergies = []
    egsTotalEnergyFractions = []
    #
    for item in allEgsFiles:#for each file
        if 'egslst' in item:#if we have a std out file
            f = open (EgsDir+item)
            output = f.read()
            lines = output.splitlines()
            x = [f.find('KINETIC') for f in lines]
            for i,index in enumerate(x):
                if index >= 0:
                    #i now stores index of command
                    command = lines[i]
                    egsEnergy=numpy.float(command.split(" ")[-2])#get energy
            egsEnergies.append(egsEnergy)
            x = [f.find('Ftot') for f in lines]
            for i,index in enumerate(x):
                if index >= 0:
                    #i now stores index of command
                    command = lines[i]
                    egsTotalEnergyFraction=numpy.float(command.split(" ")[8])#get energy
            egsTotalEnergyFractions.append(egsTotalEnergyFraction)
                       
    egsTuples=numpy.core.records.fromrecords(numpy.vstack((egsEnergies,egsTotalEnergyFractions)).transpose(), names='EgsE,EgsTEdep,EgsBkEdep')
    egsTuples.sort(order=['EgsE'])
            
    #now for the plotting
    fig=plt.figure()
    ax = fig.add_subplot(111)   
    plt.errorbar(uniqueEnergies,averageTotalEnergyFractions,yerr=stdErrTotalEnergyFractions,linestyle=':',color='b', label="Geant4")
    plt.errorbar(egsTuples['EgsE'],egsTuples['EgsTEdep'],linestyle='-',color='r', label=r"EdkNRC")
    ax.set_xscale('log')
    plt.xlabel('E, MeV')
    ax.set_ylim(ymin=0.91, ymax=1.01)
    plt.ylabel(r"total energy fraction $\epsilon_{tot}(E)$")
    plt.legend(loc=1)
    #for each data Geant4 data point
    
    plt.savefig("TotalEnergyFractionVsEnergy.png")
    
    #
    fig=plt.figure()
    ax = fig.add_subplot(111)   
    plt.errorbar(uniqueEnergies,averagePrimaryEnergyFractions,yerr=stdErrPrimaryEnergyFractions,linestyle=':',color='b', label="Geant4")
    #now plot against the NIST data, no comparison with Egs
    nistData = numpy.transpose(numpy.genfromtxt("../g4dosekernel/analysis/AttnCoeffWaterNIST.dat"))
    nistPrimaryEnergyFractions = nistData[2]/nistData[1]
    plt.errorbar(nistData[0],nistPrimaryEnergyFractions,linestyle='-',color='r', label=r"$\mu_{en}/\mu$ NIST")
    ax.set_xscale('log')
    plt.xlabel('E, MeV')
    ax.set_ylim(ymin=0, ymax=1.1)
    plt.ylabel(r"primary energy fraction $\epsilon_{pri}(E)$")
    plt.legend(loc=1)
    plt.savefig("PrimaryEnergyFractionVsEnergy.png")
    #
    
    

    
    