import sys,os
import matplotlib.pyplot as plt
from matplotlib import mpl
import numpy
from scipy.ndimage.interpolation import rotate
from scipy.signal import convolve, fftconvolve

currentDir=os.getcwd()
sys.path.append("../../G4EnergyKernel/analysis/")
resultsDirKernel='/./'

from plotkernelXYZ import plotkernelXYZ
g4=plotkernelXYZ()
g4.setCoordSystem('cartesian')
g4.saveFile = 0
g4.loadG4KernelFiles(currentDir+resultsDirKernel+"/",'4.75_MeV')#pass energy string as runID
g4.setKernelLimits()

#now define our rotation matrix
theta = 180
phi = 0
#first flip the dose kernel
#then rotate by amount
#remember, Y, X, Z, 0, 1, 2 axis indices
temp1 = rotate(g4.DoseMatrix.copy(),theta,(0,2),order=1,reshape=False, mode='nearest')
temp2 = rotate(temp1,phi+180,(0,1),order=1,reshape=False, mode='nearest')

g4.DoseMatrix=temp2
g4.setKernelLimits()
g4.calcXZplane()
g4.plotXZplane("kernelXZTheta"+numpy.str(theta)+"Phi"+numpy.str(phi)+".png")

#g4.DoseMatrix=temp2#fftconvolve(temp,kernel,mode='same')
#g4.setKernelLimits()
#g4.calcXYplane()
#g4.plotXYplane("kernelXYTheta"+numpy.str(theta)+"Phi"+numpy.str(phi)+".png")
