import sys
sys.path.append("../G4EnergyKernel/analysis/")
from plotkernelXYZ import plotkernelXYZ
p=plotkernelXYZ()
dirname="../G4EnergyKernel-build/"
p.setCoordSystem('spherical')#only available with cartesian coord system
#re-implement this once UI commands are functioning
p.parseMacroFile('../G4EnergyKernel/macros/run.mac',"/control/foreach macros/runEnergy.mac Energy")#search for energies
#for each of the kernel energies
for energy in p.Energies:
    p.loadDoseKernelnpyFiles(dirname+'/results/', energy)#pass energy string as runID
    p.doseMin=1e-17
    p.doseMax=1e-7
    p.calcRThetaPlane()
    p.calcRprofile()
    p.plotRThetaPlane()
    if p.saveFile > 0: p.saveResults(dirname)
p.plotRprofiles()#will create plots, need to actually plot with
p.plot()# ... this
