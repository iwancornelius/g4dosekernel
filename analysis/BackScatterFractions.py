import sys
import matplotlib.pyplot as plt
from matplotlib import mpl
import numpy
import os
currentDir=os.getcwd()
sys.path.append(currentDir+"/../G4DoseKernel/analysis/")
from plotkernelXYZ import plotkernelXYZ

p=plotkernelXYZ()
dirname="./"
p.setCoordSystem('spherical')#only available with cartesian coord system

backScatterEnergyFractionList = []
egsBackScaterFractions = []
energies = []
    
#re-implement this once UI commands are functioning
p.parseMacroFile('../G4DoseKernel/macros/runCollapsedCone.mac',"/control/foreach macros/runEnergy.mac Energy")#search for energies
#for each of the kernel energies
for energy in p.Energies:
    #
    print("energy ", energy)
    #get total energy kernel
    g4=plotkernelXYZ()
    g4.setCoordSystem('spherical')
    g4.Plot=0#don't plot results, just calculate
    g4.loadG4KernelFiles('./results/','_total__'+energy+'_MeV')#pass energy string as runID
    g4.calcRThetaPlane()    
    print("G4 backscatter fraction ", g4.backscatter)
    backScatterEnergyFractionList.append(g4.backscatter)
    #for all energies, open files and print back scatter and energy to file
    egsBS = plotkernelXYZ()
    egsBS.Plot=0#don't plot results, just calculate
    egsBS.setCoordSystem('spherical')
    egsBS.loadEgsKernelFile("../../edknrc_dosekernel_60cm/","edknrc_"+energy+"_60cm")
    egsBS.setKernelLimits()
    egsBS.calcRThetaPlane()
    print("Egs backscatter fraction ", egsBS.backscatter)
    egsBackScaterFractions.append(egsBS.backscatter)#scale by total energy fraction 
    energies.append(numpy.float(energy))

fig=plt.figure()
ax = fig.add_subplot(111)   
plt.plot(energies,backScatterEnergyFractionList,linestyle=':',color='b', label="Geant4")
plt.plot(energies,egsBackScaterFractions,linestyle='-',color='r', label=r"EdkNRC")
ax.set_xscale('log')
plt.xlabel('E, MeV')
#ax.set_ylim(ymin=0, ymax=0.1)
plt.ylabel(r"backscatter dose fraction $f{bck}(E)$")
plt.legend(loc=1)
plt.savefig("BackscatterEnergyFractionVsEnergy.png")



 