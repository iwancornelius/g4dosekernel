G4DoseKernel
======

Geant4 Dose Kernel calculator

- A tool that may be used to calculate dose kernels and point spread functions for monoenergetic photon beams in water
- may be run in parallel on an HPC facility using a queueing system (eg PBS)
- produces  numpy files *.npy containing the dose kernels or Point Spread Functions (PSF) in cartesian or 'collapsed cone' scoring geometries 
- see directory "analysis" for  python scripts showing examples of typical analyses

AUTHORS
======

The code is currently developed and maintained by Dr. Iwan Cornelius, a Research Fellow at the University of Wollongong, Australia; however, all contributions are welcomed and encouraged. 

email: iwan@uow.edu.au

The code is based on that of Dr. Christopher Poole:

https://github.com/christopherpoole/EnergyKernel

and extends this to include forced interactions

COPYING / LICENSE
======
Copyright (c) 2012, Dr. IWAN CORNELIUS
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.

ACKNOWLEDGEMENT
======

Scientific publications that include work derived from this code should appropriately acknowledge the authors. 
This product includes software developed by Members of the Geant4 Collaboration ( http://cern.ch/geant4 )
http://geant4.web.cern.ch/geant4/license/LICENSE.html

INSTALL
======

The software requires:
- installation and configuration of Geant4 toolkit
- installation of python, ipython, numpy, scipy, matplotlib (for analysis only)
- git version control 

Get the source code with the following command:

> git clone git@bitbucket.org:iwancornelius/g4dosekernel.git

The application is built/run as per a regular Geant4 application; ie., it is based on out-of-source builds using cmake. From the source directory type: 

> mkdir ../g4dosekernel-build 

> cd ../g4dosekernel-build 

> cmake ../g4dosekernel

> make 

then run a simulation interactively with

> ./g4dosekernel 

> /control/execute macros/run.mac 

or in batch mode with 

> ./g4dosekernel macros/run.mac 

you need to modify run.mac to specify which scoring geometry to use, as well as beam energies

check out the pbs/ directory for example scripts showing how to parallelise your simulation on a High Performance Computing facility running the PBS queueing system


OUTPUT FILES
=====

Upon completion of the simulation, a number of *.npy files (numpy), containing 2D (collapsed cone) and 3D (cartesian) distributions, will be produced in results/ directory. 

You can analyse these directly using python/numpy/scipy and there are a number of scripts in the analysis directly to show you how to collate, process, and plot the data




