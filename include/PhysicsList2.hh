

#ifndef PhysicsList2_h
#define PhysicsList2_h 1

// GEANT4 //
#include "globals.hh"
#include "G4VModularPhysicsList.hh"


class PhysicsList2: public G4VModularPhysicsList
{
public:
    PhysicsList2();
    ~PhysicsList2();
    void SetUseInteractionForcing(G4bool val){useInteractionForcing=val;}

protected:
    void ConstructParticle();
    void ConstructProcess();
    void SetCuts();

private:
    G4bool useInteractionForcing;

};

#endif

