#ifndef PhantomSD_HH
#define PhantomSD_HH

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4HCofThisEvent;
class RunAction;
class G4Track;

#include "G4ThreeVector.hh"



class PhantomSD : public G4VSensitiveDetector
{
public:
    PhantomSD(G4String);
    ~PhantomSD();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

private:
    RunAction* myRunAction;
    G4double edep;
    G4double KE;
    G4ThreeVector hitPosition;
    G4bool isPrimaryDose;
    G4bool isScattered;
    G4double edepForThisEvent;
};


#endif // PhantomSD_HH
