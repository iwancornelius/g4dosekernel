


#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1


// GEANT4 //
#include "G4VUserDetectorConstruction.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
class G4Region;
class PhantomSD;
class G4UserLimits;


class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
    void SetUseInteractionForcing(G4bool val){useInteractionForcing=val;}

private:
    G4Box* world_solid;
    G4LogicalVolume* world_logical;
    G4VPhysicalVolume* world_physical;
    G4Region* region;
    PhantomSD* aPhantomSD;
    G4UserLimits* stepLimit;// pointer to user step limits
    G4bool useInteractionForcing;
};
#endif

