#ifndef CARTESIANDOSESCORER_HH
#define CARTESIANDOSESCORER_HH 1

#include "globals.hh"
#include "G4bPIDTHitsMap.hh"
#include "G4ThreeVector.hh"
#include "DoseScorer.hh"

class CartesianDoseScorer :  public DoseScorer {
    
public:
    CartesianDoseScorer(G4String val="");
    ~CartesianDoseScorer();
    
private:
    
    G4int CalcIndex(G4double, G4double, G4double);
    G4int CalcIndex(G4int, G4int, G4int);
    
    G4int NumberXVoxels, NumberYVoxels, NumberZVoxels;
    G4double ScorerSizeX, ScorerSizeY, ScorerSizeZ;
    G4double Xmin, Xmax, Ymin, Ymax, Zmin, Zmax;
    G4double scorerXresolution, scorerYresolution,scorerZresolution;
    void PrintToFile();    
    void TallyHit(const G4ThreeVector&, G4double );
    

};
#endif // CARTESIANDOSESCORER_HH
