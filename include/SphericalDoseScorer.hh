#ifndef SPHERICALDOSESCORER_HH
#define SPHERICALDOSESCORER_HH

#include "globals.hh"
#include "G4bPIDTHitsMap.hh"
#include "G4ThreeVector.hh"
#include "DoseScorer.hh"

class SphericalDoseScorer : public DoseScorer {
    
public:
    SphericalDoseScorer(G4String val="");
    ~SphericalDoseScorer();
    
private:
    
    G4int CalcIndex(G4double, G4double);
    G4int CalcIndex(G4int, G4int);
    
    G4int NumberRVoxels, NumberThetaVoxels;
    G4double ScorerSizeR, ScorerSizeTheta;
    G4double Rmin, Rmax, ThetaMin, ThetaMax;    
    G4double scorerRresolution, scorerThetaresolution;
    
    void PrintToFile();    
    void TallyHit(const G4ThreeVector&, G4double );

};


#endif 
