#ifndef RUNACTIONMESSENGER_HH
#define RUNACTIONMESSENGER_HH

#include "globals.hh"
#include "G4UImessenger.hh"

class G4UIdirectory;
class RunAction;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;

class RunActionMessenger : public G4UImessenger
{
public:

    RunActionMessenger(RunAction* );
    ~RunActionMessenger();
    void SetNewValue(G4UIcommand* ,G4String );

private:
    G4UIcmdWithAString*        runIDFilenameCmd;
    G4UIcmdWithAString*        setScorerCmd;    
    G4UIcmdWithoutParameter*   calcDoseKernelCmd;

    RunAction*                 runAction;
    G4UIdirectory*             Dir;

};
#endif // RUNACTIONMESSENGER_HH
