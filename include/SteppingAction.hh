#ifndef STEPPINGACTION_HH
#define STEPPINGACTION_HH

#include "globals.hh"
#include "G4UserSteppingAction.hh"
#include "G4Step.hh"

class DetectorConstruction;
class RunAction;

class SteppingAction : public G4UserSteppingAction
{
public:
    SteppingAction();
    virtual ~SteppingAction();

    virtual void UserSteppingAction(const G4Step* );

private:

    RunAction* myRA;
};


#endif 
