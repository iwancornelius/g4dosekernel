#ifndef DOSESCORER_HH
#define DOSESCORER_HH 1

#include "G4bPIDTHitsMap.hh"
#include "G4ThreeVector.hh"

class DoseScorer {
    
public:
    DoseScorer();
    ~DoseScorer();
    virtual void PrintToFile();
    virtual void TallyHit(const G4ThreeVector&, G4double );
    
protected:
    
    G4bPIDTHitsMap<G4double>* doseDistribution;
    
    G4int NumberOfHistories;    
    G4double primaryPhotonEnergy;
    G4String kernelFilename;    
    G4String RunID;
    G4bool calcDoseKernel;
    G4double waterDensity;
    G4String primaryAppendix;
    G4double primaryEnergyDeposition;

    
public:
    void SetNumberHistories(G4int val){NumberOfHistories = val;};
    void ResetScorer(){doseDistribution->clear();primaryEnergyDeposition=0;};
    void SetPrimaryPhotonEnergy(G4double val){primaryPhotonEnergy=val;};
    void SetRunID(G4String val){RunID = val;};
    void CalcDoseKernel(){calcDoseKernel = true;};
    void TallyPrimaryEdep(G4double val){primaryEnergyDeposition += val;};
    
};

#endif // DOSESCORER_HH
