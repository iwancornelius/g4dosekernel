#ifndef RUNACTION
#define RUNACTION 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "DoseScorer.hh"

class RunActionMessenger;
class PrimaryGeneratorAction;

class RunAction : public G4UserRunAction {
    
public:
    RunAction(PrimaryGeneratorAction*);//to get beam energy
    ~RunAction();
    
    void BeginOfRunAction(const G4Run* aRun);
    void EndOfRunAction(const G4Run* aRun);
    
    G4bool GetUseInteractionForcing(){return useInteractionForcing;};
    void SetUseInteractionForcing(G4bool val){ useInteractionForcing=val;};
    G4ThreeVector GetInteractionOrigin() {
        return interactionOrigin;
    };
    void SetInteractionOrigin(G4ThreeVector origin) {
        interactionOrigin = origin;
    };
    
    void SetScorer(G4String);
    void SetRunID(G4String val) {myDoseScorer->SetRunID(val);};
    void CalculateDoseKernel(){myDoseScorer->CalcDoseKernel();};
    void TallyPhantomHit(const G4ThreeVector& , G4double ,G4bool);
    
private:
    RunActionMessenger* myRAM;
    G4bool useInteractionForcing;
    G4ThreeVector interactionOrigin;
    PrimaryGeneratorAction* myPGA;
    DoseScorer* myDoseScorer;
    
};

#endif
